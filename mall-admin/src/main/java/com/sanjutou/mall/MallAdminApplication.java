package com.sanjutou.mall;

import com.sanjutou.mall.security.config.IgnoreUrlsConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * 应用启动入口
 * Created by sunliang on 2022/6/11.
 */

//@ComponentScan(basePackages = {"com.sanjutou.mall.security.component","com.sanjutou.mall.admin","com.sanjutou.mall.security.utils","com.sanjutou.mall.security.config","com.sanjutou.mall.common.utils"})
@SpringBootApplication
@EnableConfigurationProperties({IgnoreUrlsConfig.class})
public class MallAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(MallAdminApplication.class, args);
    }
}
