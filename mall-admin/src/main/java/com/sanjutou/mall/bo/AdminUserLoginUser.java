package com.sanjutou.mall.bo;
import com.sanjutou.mall.entity.UmsResource;
import com.sanjutou.mall.entity.UmsAdmin;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * UserDetails具体实现类，为了封装成UserDetails对象交给spring security
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminUserLoginUser implements UserDetails {

    // 后台用户
    private UmsAdmin umsAdmin;
    // 拥有的权限列表
    private List<UmsResource> resourceList;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //返回当前用户的角色
        return resourceList.stream()
                .map(role -> new SimpleGrantedAuthority(role.getId()+":"+ role.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return umsAdmin.getPassword();
    }

    @Override
    public String getUsername() {
        return umsAdmin.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
       return umsAdmin.getStatus().equals(1);
    }
}
