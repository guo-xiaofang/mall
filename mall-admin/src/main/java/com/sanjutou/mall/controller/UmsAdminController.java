package com.sanjutou.mall.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sanjutou.mall.common.api.CommonResult;
import com.sanjutou.mall.dto.UmsAdminLoginParam;
import com.sanjutou.mall.dto.UmsAdminRegisterParam;
import com.sanjutou.mall.entity.UmsAdmin;
import com.sanjutou.mall.entity.UmsRole;
import com.sanjutou.mall.service.UmsAdminService;
import com.sanjutou.mall.service.UmsRoleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin")
public class UmsAdminController {
    @Value("${jwt.tokenHeader}")
    private String tokenHeader;
    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @Autowired
    private UmsAdminService umsAdminService;

    @Autowired
    private UmsRoleService roleService;

    /**
     * 用户登陆
     */
    @ApiOperation("用户登陆")
    @PostMapping("/login")
    public CommonResult loginIN(@Validated @RequestBody UmsAdminLoginParam umsAdminLoginParam) {
        String token = umsAdminService.login(umsAdminLoginParam.getUsername(), umsAdminLoginParam.getPassword());
        Map<String, String> tokenMap = new HashMap<>();
        tokenMap.put("token", token);
        tokenMap.put("tokenHead", tokenHead);
        return CommonResult.success(tokenMap);
    }

    /**
     * 用户注册或修改
     */
    @ApiOperation("用户注册")
    @PostMapping("/register")
    public CommonResult register(@Validated @RequestBody UmsAdminRegisterParam umsAdminRegisterParam) {
        if (ObjectUtil.isEmpty(umsAdminRegisterParam.getId())) {
            int count = umsAdminService.register(umsAdminRegisterParam);
            if (count >= 1) {
                return CommonResult.success("注册成功");
            }
            return CommonResult.failed("注册失败");
        } else {
            int count = umsAdminService.updateId(umsAdminRegisterParam);
            if (count >= 1) {
                return CommonResult.success("修改成功");
            }
            return CommonResult.failed("修改失败");
        }
    }

    /**
     * 分页查询用户列表
     */
    @ApiOperation("根据用户名或姓名分页获取用户列表")
    @GetMapping("/list")
    public CommonResult listPage(@RequestParam(value = "keyword", required = false) String keyword,
                                 @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                 @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        IPage<UmsAdmin> list = umsAdminService.listPage(keyword, pageSize, pageNum);
        return CommonResult.success(list);
    }

    /**
     * 查询用户详细信息
     */
    @ApiOperation("查询用户详细信息")
    @GetMapping("/info")
    public CommonResult getAdminInfo(Principal principal) {
        if (principal == null) {
            return CommonResult.unauthorized(null);
        }
        String username = principal.getName();
        UmsAdmin umsAdmin = umsAdminService.getAdminByUsername(username);
        Map<String, Object> data = new HashMap<>();
        data.put("username", umsAdmin.getUsername());
        data.put("menus", roleService.getMenuList(umsAdmin.getId()));
        data.put("icon", umsAdmin.getIcon());
        List<UmsRole> roleList = umsAdminService.getRoleList(umsAdmin.getId());
        if (CollUtil.isNotEmpty(roleList)) {
            List<String> roles = roleList.stream().map(UmsRole::getName).collect(Collectors.toList());
            data.put("roles", roles);
        }
        return CommonResult.success(data);
    }

    /**
     * 删除指定用户信息
     */
    @ApiOperation("删除指定用户信息")
    @PostMapping("/delete/{id}")
    public CommonResult delete(@PathVariable Long id) {
        int count = umsAdminService.delete(id);
        if (count > 0) {
            return CommonResult.success(count, "删除成功");
        }
        return CommonResult.failed("删除失败");
    }

    /**
     * 修改帐号状态
     */
    @ApiOperation("修改帐号状态")
    @PostMapping("/updateStatus/{id}")
    public CommonResult updateStatus(@PathVariable Long id, @RequestParam(value = "status") Integer status) {
        UmsAdmin umsAdmin = new UmsAdmin();
        umsAdmin.setId(id);
        umsAdmin.setStatus(status);
        boolean isSuccess = umsAdminService.updateById(umsAdmin);
        if (isSuccess) {
            return CommonResult.success("修改成功");
        }
        return CommonResult.failed("修改失败");
    }

    /**
     * 根据用户id获取指定用户的角色
     */
    @ApiOperation("获取指定用户的角色")
    @GetMapping(value = "/role/{adminId}")
    public CommonResult<List<UmsRole>> getRoleListByAdminId(@PathVariable Long adminId) {
        List<UmsRole> roleList = umsAdminService.getRoleListByAdminId(adminId);
        return CommonResult.success(roleList);
    }

    /**
     * 给用户分配角色
     */
    @ApiOperation("给用户分配角色")
    @PostMapping(value = "/role/update")
    public CommonResult updateRole(@RequestParam("adminId") Long adminId,
                                   @RequestParam("roleIds") List<Long> roleIds) {
        int count = umsAdminService.updateRole(adminId, roleIds);
        if (count >= 0) {
            return CommonResult.success("分配成功");
        }
        return CommonResult.failed("分配失败");
    }

    @ApiOperation(value = "登出功能")
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    @ResponseBody
    public CommonResult logout() {
        return CommonResult.success(null);
    }

}
