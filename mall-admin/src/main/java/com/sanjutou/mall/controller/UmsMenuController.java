package com.sanjutou.mall.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sanjutou.mall.common.api.CommonResult;
import com.sanjutou.mall.dto.UmsMenuCreateParam;
import com.sanjutou.mall.entity.UmsMenu;
import com.sanjutou.mall.service.UmsMenuService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/menu")
public class UmsMenuController {

    @Autowired
    private UmsMenuService umsMenuService;

    /**
     * 查询菜单树
     */
    @ApiOperation("查询菜单树")
    @GetMapping("/treeList")
    public CommonResult<List<UmsMenu>> treeList() {
        List<UmsMenu> list = umsMenuService.treeList();
        return CommonResult.success(list);
    }

    /**
     * 分页查询后台菜单
     */
    @ApiOperation("分页查询后台菜单")
    @GetMapping(value = "/list/{parentId}")
    public CommonResult<IPage<UmsMenu>> list(@PathVariable Long parentId,
                                             @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                             @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        IPage<UmsMenu> listPage = umsMenuService.list(parentId, pageSize, pageNum);
        return CommonResult.success(listPage);
    }

    /**
     * 通过id查询菜单回显数据
     */
    @ApiOperation("通过id查询菜单回显数据")
    @GetMapping(value = "{id}")
    public CommonResult<UmsMenu> oneById(@PathVariable("id") Long id) {
        return CommonResult.success(umsMenuService.oneById(id));
    }

    /**
     * 添加或修改菜单数据
     */
    @ApiOperation("添加或修改菜单数据")
    @PostMapping(value = "createOrUpdate")
    public CommonResult crateOrUpdateMenu(@RequestBody UmsMenuCreateParam umsMenuCreateParam) {
        int count;
        if (ObjectUtil.isNotEmpty(umsMenuCreateParam.getId())) {
            count = umsMenuService.update(umsMenuCreateParam);
            if (count >= 1) {
                return CommonResult.success("修改成功");
            }
            return CommonResult.failed("修改失败");
        } else {
            count = umsMenuService.create(umsMenuCreateParam);
            if (count >= 1) {
                return CommonResult.success("新增成功");
            }
            return CommonResult.failed("新增失败");
        }

    }
    /**
     * 删除指定菜单信息
     */
    @ApiOperation("删除指定用户信息")
    @PostMapping("/delete/{id}")
    public CommonResult delete(@PathVariable Long id) {
        int count = umsMenuService.delete(id);
        if (count > 0) {
            return CommonResult.success(count,"删除成功");
        }
        return CommonResult.failed("删除失败");
    }

    /**
     * 修改菜单前端是否隐藏
     */
    @ApiOperation("修改菜单前端是否隐藏")
    @PostMapping("/updateHidden/{id}")
    public CommonResult updateHidden(@PathVariable Long id,@RequestParam("hidden") Integer hidden) {
        int count = umsMenuService.updateHidden(id,hidden);
        if (count > 0) {
            return CommonResult.success(count,"修改成功");
        }
        return CommonResult.failed("修改失败");
    }
}
