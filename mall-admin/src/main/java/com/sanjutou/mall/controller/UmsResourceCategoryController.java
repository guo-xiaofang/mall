package com.sanjutou.mall.controller;

import cn.hutool.core.util.ObjectUtil;
import com.sanjutou.mall.common.api.CommonResult;
import com.sanjutou.mall.dto.UmsResourceCategoryCreateParam;
import com.sanjutou.mall.dto.UmsResourceCreateParam;
import com.sanjutou.mall.entity.UmsResource;
import com.sanjutou.mall.entity.UmsResourceCategory;
import com.sanjutou.mall.service.UmsResourceCategoryService;
import com.sanjutou.mall.service.UmsResourceService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/resourceCategory")
@RestController
public class UmsResourceCategoryController {

    @Autowired
    private UmsResourceCategoryService umsResourceCategoryService;

    /**
     * 获取所有资源分类
     */
    @ApiOperation("获取所有资源分类")
    @GetMapping("/listAll")
    public CommonResult<List<UmsResourceCategory>> getRoleList() {
        List<UmsResourceCategory> umsResourceCategories = umsResourceCategoryService.listAll();
        return CommonResult.success(umsResourceCategories);
    }

    /**
     * 新增资源分类、更改资源分类
     */
    @ApiOperation("新增资源分类、更改资源分类")
    @PostMapping("/createOrUpdate")
    public CommonResult createOrUpdate(@Validated @RequestBody UmsResourceCategoryCreateParam resourceCategoryCreateParam) {
        int count;
        if (ObjectUtil.isNotEmpty(resourceCategoryCreateParam.getId())) {
            count =  umsResourceCategoryService.update(resourceCategoryCreateParam);
            if (count >= 1) {
                return CommonResult.success("修改成功");
            }
            return CommonResult.failed("修改失败");
        }else{
            count = umsResourceCategoryService.create(resourceCategoryCreateParam);
            if (count >= 1) {
                return CommonResult.success("新增成功");
            }
            return  CommonResult.failed("新增失败");
        }
    }

    /**
     * 删除指定资源信息
     */
    @ApiOperation("删除指定资源信息")
    @PostMapping("/delete/{id}")
    public CommonResult delete(@PathVariable Long id) {
        int count = umsResourceCategoryService.delete(id);
        if (count > 0) {
            return CommonResult.success(count,"删除成功");
        }
        return CommonResult.failed("删除失败");
    }
}
