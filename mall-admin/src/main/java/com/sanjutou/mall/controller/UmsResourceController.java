package com.sanjutou.mall.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sanjutou.mall.common.api.CommonResult;
import com.sanjutou.mall.dto.UmsResourceCreateParam;
import com.sanjutou.mall.entity.UmsResource;
import com.sanjutou.mall.security.component.DynamicSecurityMetadataSource;
import com.sanjutou.mall.service.UmsResourceService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/resource")
@RestController
public class UmsResourceController {

    @Autowired
    private UmsResourceService umsResourceService;

    @Autowired
    private DynamicSecurityMetadataSource dynamicSecurityMetadataSource;

    /**
     * 获取所有资源
     */
    @ApiOperation("获取所有资源")
    @GetMapping("/listAll")
    public CommonResult<List<UmsResource>> getRoleList() {
        List<UmsResource> umsRoles = umsResourceService.listAll();
        return CommonResult.success(umsRoles);
    }

    /**
     * 分页查询资源列表
     */
    @ApiOperation("分页查询资源列表")
    @GetMapping("/list")
    public CommonResult<IPage<UmsResource>> listPage(@RequestParam(value = "nameKeyword", required = false) String nameKeyword,
                                                     @RequestParam(value = "urlKeyword", required = false) String urlKeyword,
                                                     @RequestParam(value = "categoryId", required = false) Integer categoryId,
                                                     @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                                     @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        IPage<UmsResource> umsResourceIPage = umsResourceService.listPage(nameKeyword, urlKeyword, categoryId, pageSize, pageNum);
        return CommonResult.success(umsResourceIPage);
    }

    /**
     * 新增资源、更改资源
     */
    @ApiOperation("新增资源、更改资源")
    @PostMapping("/createOrUpdate")
    public CommonResult createOrUpdate(@Validated @RequestBody UmsResourceCreateParam umsResourceCre) {
        int count;
        if (ObjectUtil.isNotEmpty(umsResourceCre.getId())) {
            count = umsResourceService.update(umsResourceCre);
            dynamicSecurityMetadataSource.clearDataSource();
            if (count >= 1) {
                return CommonResult.success("修改成功");
            }
            return CommonResult.failed("修改失败");
        } else {
            count = umsResourceService.create(umsResourceCre);
            dynamicSecurityMetadataSource.clearDataSource();
            if (count >= 1) {
                return CommonResult.success("新增成功");
            }
            return CommonResult.failed("新增失败");
        }
    }

    /**
     * 删除指定资源信息
     */
    @ApiOperation("删除指定资源信息")
    @PostMapping("/delete/{id}")
    public CommonResult delete(@PathVariable Long id) {
        int count = umsResourceService.delete(id);
        dynamicSecurityMetadataSource.clearDataSource();
        if (count > 0) {
            return CommonResult.success(count, "删除成功");
        }
        return CommonResult.failed("删除失败");
    }
}
