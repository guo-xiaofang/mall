package com.sanjutou.mall.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sanjutou.mall.common.api.CommonResult;
import com.sanjutou.mall.dto.UmsRoleCreateParam;
import com.sanjutou.mall.entity.UmsMenu;
import com.sanjutou.mall.entity.UmsResource;
import com.sanjutou.mall.entity.UmsRole;
import com.sanjutou.mall.service.UmsRoleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/role")
public class UmsRoleController {

    @Autowired
    private UmsRoleService umsRoleService;

    /**
     * 获取所有角色
     *
     */
    @ApiOperation("获取所有角色")
    @GetMapping("/listAll")
    public CommonResult<List<UmsRole>> getRoleList() {
        List<UmsRole> umsRoles = umsRoleService.listAll();
        return CommonResult.success(umsRoles);
    }

    /**
     * 分页查询角色列表
     */
    @ApiOperation("分页查询角色列表")
    @GetMapping("/list")
    public CommonResult listPage(@RequestParam(value = "keyword", required = false) String keyword,
                                 @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                 @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        IPage<UmsRole> list = umsRoleService.listPage(keyword, pageSize, pageNum);
        return CommonResult.success(list);
    }

    /**
     * 新增角色、更改角色
     */
    @ApiOperation("新增角色、更改角色")
    @PostMapping("/createOrUpdate")
    public CommonResult createOrUpdate(@Validated @RequestBody UmsRoleCreateParam umsRoleCre) {
        int count;
        if (ObjectUtil.isNotEmpty(umsRoleCre.getId())) {
            count =  umsRoleService.update(umsRoleCre);
            if (count >= 1) {
                return CommonResult.success("修改成功");
            }
            return CommonResult.failed("修改失败");
        }else{
            count = umsRoleService.create(umsRoleCre);
            if (count >= 1) {
                return CommonResult.success("新增成功");
            }
           return  CommonResult.failed("新增失败");
        }
    }

    /**
     * 删除指定用户信息
     */
    @ApiOperation("删除指定用户信息")
    @PostMapping("/delete/{id}")
    public CommonResult delete(@PathVariable Long id) {
        int count = umsRoleService.delete(id);
        if (count > 0) {
            return CommonResult.success(count,"删除成功");
        }
        return CommonResult.failed("删除失败");
    }

    /**
     * 修改用户状态
     */
    @ApiOperation("修改用户状态")
    @PostMapping("/updateStatus/{id}")
    public CommonResult delete(@PathVariable Long id,@RequestParam("status") Integer status) {
        int count = umsRoleService.updateStatus(id,status);
        if (count > 0) {
            return CommonResult.success(count,"修改成功");
        }
        return CommonResult.failed("修改失败");
    }

    /**
     * 查询该角色的菜单信息
     */
    @ApiOperation("查询该用户的菜单信息")
    @GetMapping("/listMenu/{id}")
    public CommonResult<List<UmsMenu>> listMenuByRole(@PathVariable Long id) {
        List<UmsMenu> list = umsRoleService.listMenuByRole(id);
        return CommonResult.success(list);
    }

    /**
     * 给角色分配菜单
     */
    @ApiOperation("给角色分配菜单")
    @PostMapping("/allocMenu")
    public CommonResult allocMenu(@RequestParam("roleId") Long roleId, @RequestParam("menuIds") List<Long> menuIds) {
        int count  = umsRoleService.allocMenu(roleId,menuIds);
        if(count >= 0) {
            return CommonResult.success("分配成功");
        }
        return CommonResult.success("分配失败");
    }

    /**
     * 给角色分配资源
     */
    @ApiOperation("给角色分配资源")
    @PostMapping("/allocResource")
    public CommonResult allocResource(@RequestParam("roleId") Long roleId, @RequestParam("resourceIds") List<Long> resourceIds) {
        int count  = umsRoleService.allocResource(roleId,resourceIds);
        if(count >= 0) {
            return CommonResult.success("分配成功");
        }
        return CommonResult.success("分配失败");
    }

    /**
     * 查询该角色的菜单信息
     */
    @ApiOperation("查询该用户的菜单信息")
    @GetMapping("/listResource/{id}")
    public CommonResult<List<UmsResource>> listResourceByRole(@PathVariable Long id) {
        List<UmsResource> list = umsRoleService.listResourceByRole(id);
        return CommonResult.success(list);
    }
}
