package com.sanjutou.mall.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 菜单新建或修改参数
 * Created by sunliang on 2022/7/20.
 */
@Data
public class UmsMenuCreateParam {

    @ApiModelProperty(value = "主键")
    private Long id;

    @NotBlank(message = "请输入菜单名称")
    @Size(min = 2,max = 140)
    @ApiModelProperty(value = "菜单名称")
    private String title;

    @ApiModelProperty(value = "父级ID")
    private Long parentId;

    @NotBlank(message = "请输入前端名称")
    @Size(min = 2,max = 140)
    @ApiModelProperty(value = "前端名称")
    private String name;

    @NotBlank(message = "请输入前端图标")
    @Size(min = 2,max = 140)
    @ApiModelProperty(value = "前端图标")
    private String icon;

    @ApiModelProperty(value = "前端隐藏")
    private Integer hidden;

    @ApiModelProperty(value = "菜单排序")
    private Integer sort;
}
