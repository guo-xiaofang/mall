package com.sanjutou.mall.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UmsResourceCategoryCreateParam {

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "分类名称")
    private String name ;

    @ApiModelProperty(value = "排序")
    private Integer sort;
}
