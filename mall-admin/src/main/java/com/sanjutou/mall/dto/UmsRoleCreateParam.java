package com.sanjutou.mall.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 角色新建或修改参数
 * Created by sunliang on 2022/7/20.
 */
@Data
public class UmsRoleCreateParam {

    private Long id;

    @NotBlank(message = "角色名不能为空")
    @ApiModelProperty(value = "角色名")
    private String name;

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "启用状态：0->禁用；1->启用")
    private Integer status;

}
