package com.sanjutou.mall.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("ums_admin_role_relation")
public class UmsAdminRoleRelation implements Serializable {

    /**
     * 序列号
     */
    private static final long serialVersionUID = -6734606129155860659L;

    @TableId
    private Long id;

    @ApiModelProperty(value = "用户id")
    private Long adminId;

    @ApiModelProperty(value = "角色id")
    private Long roleId;


}