package com.sanjutou.mall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sanjutou.mall.entity.UmsAdmin;
import com.sanjutou.mall.entity.UmsRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UmsAdminMapper extends BaseMapper<UmsAdmin> {

    /**
     * 根据用id获取该用户所有角色
     */
    List<UmsRole> getRoleListByAdminId(@Param("adminId") Long adminId);

    UmsAdmin selectByPrimaryKey(Long id);
}
