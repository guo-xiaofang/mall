package com.sanjutou.mall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sanjutou.mall.entity.UmsAdminRoleRelation;
import com.sanjutou.mall.entity.UmsResource;
import com.sanjutou.mall.entity.UmsRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UmsAdminRoleRelationMapper extends BaseMapper<UmsAdminRoleRelation> {
    /**
     * 获取用户的所有权限信息
     */
    List<UmsResource> getResourceList(@Param("adminId") Long id);

    /**
     * 获取用户所有角色
     */
    List<UmsRole> getRoleList(@Param("adminId") Long adminId);

    /**
     * 给用户分配角色
     */
    int insertBatch(List<UmsAdminRoleRelation> list);

    /**
     * 获取资源相关用户ID列表
     */
    List<Long> getAdminIdList(Long resourceId);
}
