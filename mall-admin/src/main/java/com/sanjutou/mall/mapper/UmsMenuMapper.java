package com.sanjutou.mall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sanjutou.mall.entity.UmsMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UmsMenuMapper extends BaseMapper<UmsMenu> {

    /**
     * 修改菜单前端是否隐藏
     */
    int updateHidden(@Param("id") Long id, @Param("hidden") Integer hidden);
}
