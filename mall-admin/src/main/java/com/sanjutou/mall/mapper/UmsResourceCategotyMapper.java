package com.sanjutou.mall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sanjutou.mall.entity.UmsResourceCategory;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UmsResourceCategotyMapper extends BaseMapper<UmsResourceCategory> {
}
