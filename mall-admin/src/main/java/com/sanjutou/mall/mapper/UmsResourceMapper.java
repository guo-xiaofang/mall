package com.sanjutou.mall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sanjutou.mall.entity.UmsResource;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UmsResourceMapper extends BaseMapper<UmsResource> {

}
