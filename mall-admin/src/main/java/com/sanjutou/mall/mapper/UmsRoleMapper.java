package com.sanjutou.mall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sanjutou.mall.entity.UmsMenu;
import com.sanjutou.mall.entity.UmsResource;
import com.sanjutou.mall.entity.UmsRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UmsRoleMapper extends BaseMapper<UmsRole> {

    /**
     * 根据后台用户ID获取菜单
     */
    List<UmsMenu> getMenuList(@Param("adminId") Long adminId);
    /**
     * 修改角色状态
     */
    int updateStatus(@Param("id") Long id, @Param("status") Integer status);

    /**
     * 查询该角色的菜单信息
     */
    List<UmsMenu> listMenuByRole(@Param("id") Long id);

    /**
     * 查询该角色拥有的访问资源
     */
    List<UmsResource> listResourceByRole(Long id);
}
