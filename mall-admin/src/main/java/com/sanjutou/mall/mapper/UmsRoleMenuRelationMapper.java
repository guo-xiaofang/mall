package com.sanjutou.mall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sanjutou.mall.entity.UmsRoleMenuRelation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UmsRoleMenuRelationMapper extends BaseMapper<UmsRoleMenuRelation> {
    /**
     * 给角色分配菜单
     */
    int insertBatch(@Param("umsRoleMenuRelations") List<UmsRoleMenuRelation> umsRoleMenuRelations);
}
