package com.sanjutou.mall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fasterxml.jackson.databind.ser.Serializers;
import com.sanjutou.mall.entity.UmsResource;
import com.sanjutou.mall.entity.UmsRoleResourceRelation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UmsRoleResourceRelationMapper extends BaseMapper<UmsRoleResourceRelation> {
    /**
     * 给角色分配访问资源
     */
    int insertBatch(@Param("umsRoleResourceRelation") List<UmsRoleResourceRelation> umsRoleResourceRelation);


}
