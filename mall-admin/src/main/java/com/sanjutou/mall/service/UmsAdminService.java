package com.sanjutou.mall.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sanjutou.mall.dto.UmsAdminRegisterParam;
import com.sanjutou.mall.entity.UmsAdmin;
import com.sanjutou.mall.entity.UmsResource;
import com.sanjutou.mall.entity.UmsRole;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

public interface UmsAdminService extends IService<UmsAdmin> {
    /**
     * 用户登陆
     */
    String login(String username, String password);

    /**
     * 通过姓名获取用户信息
     */
    UserDetails loadUserByUsername(String username);

    /**
     * 根据用户名获取后台管理员
     */
    UmsAdmin getAdminByUsername(String username);

    /**
     * 获取用户对应角色
     */
    List<UmsRole> getRoleList(Long adminId);

    /**
     * 分页查询用户列表
     */
    IPage<UmsAdmin> listPage(String keyword, Integer pageSize, Integer pageNum);

    /**
     * 删除用户指定信息
     */
    int delete(Long id);

    /**
     * 用户注册
     */
    int register(UmsAdminRegisterParam umsAdminRegisterParam);

    /**
     * 用户修改
     */
    int updateId(UmsAdminRegisterParam umsAdminRegisterParam);

    /**
     * 根据用户id获取指定用户的角色
     */
    List<UmsRole> getRoleListByAdminId(Long adminId);

    /**
     * 给用户分配角色
     */
    int updateRole(Long adminId, List<Long> roleIds);

    /**
     * 获取指定用户的可访问资源
     */
    List<UmsResource> getResourceList(Long adminId);

    /**
     * 根据用户id获取用户
     */
    UmsAdmin getItem(Long adminId);
}
