package com.sanjutou.mall.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sanjutou.mall.dto.UmsMenuCreateParam;
import com.sanjutou.mall.entity.UmsMenu;

import java.util.List;

public interface UmsMenuService {
    /**
     * 查询菜单树
     */
    List<UmsMenu> treeList();

    /**
     * 分页查询后台菜单
     */
    IPage<UmsMenu> list(Long parentId, Integer pageSize, Integer pageNum);

    /**
     * 通过id查询菜单回显数据
     */
    UmsMenu oneById(Long id);

    /**
     * 新增菜单
     */
    int create(UmsMenuCreateParam umsMenuCreateParam);

    /**
     * 修改菜单
     */
    int update(UmsMenuCreateParam umsMenuCreateParam);

    /**
     * 删除指定菜单信息
     */
    int delete(Long id);

    /**
     * 修改菜单前端是否隐藏
     */
    int updateHidden(Long id, Integer hidden);
}
