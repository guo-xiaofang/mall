package com.sanjutou.mall.service;

import com.sanjutou.mall.dto.UmsResourceCategoryCreateParam;
import com.sanjutou.mall.entity.UmsResourceCategory;

import java.util.List;

public interface UmsResourceCategoryService {
    /**
     * 获取所有资源分类
     */
    List<UmsResourceCategory> listAll();

    /**
     * 修改资源分类
     */
    int update(UmsResourceCategoryCreateParam resourceCategoryCreateParam);

    /**
     * 新增资源分类
     */
    int create(UmsResourceCategoryCreateParam resourceCategoryCreateParam);

    /**
     * 删除指定资源信息
     */
    int delete(Long id);
}
