package com.sanjutou.mall.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sanjutou.mall.dto.UmsResourceCreateParam;
import com.sanjutou.mall.entity.UmsResource;

import java.util.List;

public interface UmsResourceService {

    /**
     * 查询全部资源
     */
    List<UmsResource> listAll();

    /**
     * 分页查询资源列表
     */
    IPage<UmsResource> listPage(String nameKeyword, String urlKeyword, Integer categoryId, Integer pageSize, Integer pageNum);

    /**
     * 更新资源
     */
    int update(UmsResourceCreateParam umsResourceCre);

    /**
     * 新建资源
     */
    int create(UmsResourceCreateParam umsResourceCre);

    /**
     * 删除指定资源信息
     */
    int delete(Long id);
}
