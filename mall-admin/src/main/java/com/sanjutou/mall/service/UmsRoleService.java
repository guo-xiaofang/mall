package com.sanjutou.mall.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sanjutou.mall.dto.UmsRoleCreateParam;
import com.sanjutou.mall.entity.UmsMenu;
import com.sanjutou.mall.entity.UmsResource;
import com.sanjutou.mall.entity.UmsRole;

import java.util.List;

public interface UmsRoleService {

    /**
     * 根据管理员ID获取对应菜单
     */
    List<UmsMenu> getMenuList(Long adminId);

    /**
     * 获取所有角色
     */
    List<UmsRole> listAll();

    /**
     * 分页查询角色列表
     */
    IPage<UmsRole> listPage(String keyword, Integer pageSize, Integer pageNum);

    /**
     * 新增角色
     */
    int create(UmsRoleCreateParam umsRoleCre);

    /**
     * 修改角色
     */
    int update(UmsRoleCreateParam umsRoleCre);

    /**
     * 删除角色
     */
    int delete(Long id);

    /**
     * 修改用户状态
     */
    int updateStatus(Long id, Integer status);

    /**
     * 查询该角色的菜单信息
     */
    List<UmsMenu> listMenuByRole(Long id);

    /**
     * 给角色分配菜单
     */
    int allocMenu(Long roleId, List<Long> menuIds);

    /**
     * 给角色分配资源
     */
    int allocResource(Long roleId, List<Long> resourceIds);

    /**
     * 查询该角色的资源信息
     * @param id
     * @return
     */
    List<UmsResource> listResourceByRole(Long id);
}
