package com.sanjutou.mall.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sanjutou.mall.bo.AdminUserLoginUser;
import com.sanjutou.mall.dto.UmsAdminLoginParam;
import com.sanjutou.mall.dto.UmsAdminRegisterParam;
import com.sanjutou.mall.entity.UmsAdmin;
import com.sanjutou.mall.entity.UmsAdminRoleRelation;
import com.sanjutou.mall.entity.UmsResource;
import com.sanjutou.mall.entity.UmsRole;
import com.sanjutou.mall.mapper.UmsAdminMapper;
import com.sanjutou.mall.mapper.UmsAdminRoleRelationMapper;
import com.sanjutou.mall.common.exception.ApiException;
import com.sanjutou.mall.service.UmsAdminCacheService;
import com.sanjutou.mall.service.UmsAdminService;
import com.sanjutou.mall.security.utils.JwtTokenUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UmsAdminServiceImpl extends ServiceImpl<UmsAdminMapper, UmsAdmin> implements UmsAdminService {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UmsAdminMapper umsAdminMapper;

    @Autowired
    private UmsAdminRoleRelationMapper umsAdminRoleRelationMapper;

    @Autowired
    private UmsAdminCacheService umsAdminCacheService;


    /**
     * 用户登陆
     */
    @Override
    public String login(String username, String password) {
        UserDetails userDetails = this.loadUserByUsername(username);
        if (!passwordEncoder.matches(password, userDetails.getPassword())) {
            throw new ApiException("密码不正确");
        }
        if (!userDetails.isEnabled()) {
            throw new ApiException("账号已被禁用");
        }
        String token = jwtTokenUtil.generateToken(userDetails);
        return token;
    }

    /**
     * 通过姓名获取用户信息
     */
    @Override
    public UserDetails loadUserByUsername(String username) {
        UmsAdmin umsAdmin = getAdminByUsername(username);
        // 如果存在用户那就查询该用户的权限信息并封装到AdminUserLoginUser中返回
        if (ObjectUtil.isNotNull(umsAdmin)) {
            List<UmsResource> resources = getResourceList(umsAdmin.getId());
            return new AdminUserLoginUser(umsAdmin, resources);
        }
        throw new ApiException("用户名或密码错误");
    }

    /**
     * 获取指定用户的可访问资源
     */
    @Override
    public List<UmsResource> getResourceList(Long adminId) {
        List<UmsResource> resourceList = umsAdminCacheService.getResourceList(adminId);
        if (CollUtil.isNotEmpty(resourceList)) {
            return resourceList;
        }
        resourceList = umsAdminRoleRelationMapper.getResourceList(adminId);
        if (CollUtil.isNotEmpty(resourceList)) {
            umsAdminCacheService.setResourceList(adminId, resourceList);
        }
        return resourceList;
    }

    /**
     * 根据用户名获取后台管理员
     */
    @Override
    public UmsAdmin getAdminByUsername(String username) {
        // 如果缓存中有直接返回
        UmsAdmin admin = umsAdminCacheService.getAdmin(username);
        if (admin != null) {
            return admin;
        }
        LambdaQueryWrapper<UmsAdmin> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(UmsAdmin::getUsername, username);
        List<UmsAdmin> umsAdmins = umsAdminMapper.selectList(lambdaQueryWrapper);
        if (!CollectionUtils.isEmpty(umsAdmins)) {
            admin = umsAdmins.get(0);
            // 如果缓存中没有去数据库中查并向缓存中插入数据
            umsAdminCacheService.setAdmin(admin);
            return admin;
        }
        return null;
    }

    /**
     * 获取用户对应角色
     */
    @Override
    public List<UmsRole> getRoleList(Long adminId) {
        return umsAdminRoleRelationMapper.getRoleList(adminId);
    }

    /**
     * 根据用户名或姓名分页获取用户列表
     */
    @Override
    public IPage<UmsAdmin> listPage(String keyword, Integer pageSize, Integer pageNum) {
        QueryWrapper<UmsAdmin> wrapper = new QueryWrapper<>();
        wrapper.like(StrUtil.isNotBlank(keyword), "nick_name", keyword).or().like(StrUtil.isNotBlank(keyword),"username", keyword);
        Page<UmsAdmin> page = new Page<>(pageNum, pageSize);
        IPage<UmsAdmin> umsAdminIPage = umsAdminMapper.selectPage(page, wrapper);
        return umsAdminIPage;
    }

    /**
     * 删除用户指定信息
     */
    @Override
    public int delete(Long id) {
        umsAdminCacheService.delResourceList(id);
        return umsAdminMapper.deleteById(id);
    }

    /**
     * 用户注册
     */
    @Override
    public int register(UmsAdminRegisterParam umsAdminRegisterParam) {
        UmsAdmin umsAdmin = new UmsAdmin();
        QueryWrapper<UmsAdmin> wrapper = new QueryWrapper<>();
        wrapper.eq("nick_name", umsAdminRegisterParam.getNickName());
        List<UmsAdmin> umsAdmins = umsAdminMapper.selectList(wrapper);
        if (CollectionUtil.isNotEmpty(umsAdmins)) {
            throw new ApiException("用户名已存在");
        }
        BeanUtil.copyProperties(umsAdminRegisterParam, umsAdmin);
        umsAdmin.setPassword(passwordEncoder.encode(umsAdminRegisterParam.getPassword()));
        umsAdmin.setCreateTime(new Date());
        return umsAdminMapper.insert(umsAdmin);
    }

    /**
     * 用户修改
     */
    @Override
    public int updateId(UmsAdminRegisterParam umsAdminRegisterParam) {
        UmsAdmin umsAdmin = new UmsAdmin();
        QueryWrapper<UmsAdmin> wrapper = new QueryWrapper<>();
        wrapper.eq("nick_name", umsAdminRegisterParam.getNickName());
        wrapper.ne("id",umsAdminRegisterParam.getId());
        List<UmsAdmin> umsAdmins = umsAdminMapper.selectList(wrapper);
        if (CollectionUtil.isNotEmpty(umsAdmins)) {
            throw new ApiException("用户姓名已存在");
        }
        BeanUtil.copyProperties(umsAdminRegisterParam, umsAdmin);
        umsAdmin.setPassword(passwordEncoder.encode(umsAdminRegisterParam.getPassword()));
        umsAdminCacheService.delResourceList(umsAdminRegisterParam.getId());
        return umsAdminMapper.updateById(umsAdmin);

    }

    /**
     * 根据用户id获取指定用户的角色
     */
    @Override
    public List<UmsRole> getRoleListByAdminId(Long adminId) {
        return umsAdminMapper.getRoleListByAdminId(adminId);
    }

    /**
     * 给用户分配角色
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateRole(Long adminId, List<Long> roleIds) {
        int count = roleIds == null ? 0 : roleIds.size();
        // 先删除原来的关系
        QueryWrapper<UmsAdminRoleRelation> wrapper = new QueryWrapper<>();
        wrapper.eq("admin_id", adminId);
        umsAdminRoleRelationMapper.delete(wrapper);
        if (CollectionUtil.isNotEmpty(roleIds)) {
            List<UmsAdminRoleRelation> list = new ArrayList<>();
            for (Long roleId : roleIds) {
                UmsAdminRoleRelation arrelation = new UmsAdminRoleRelation();
                arrelation.setAdminId(adminId);
                arrelation.setRoleId(roleId);
                list.add(arrelation);
            }
            return umsAdminRoleRelationMapper.insertBatch(list);
        }
        return count;
    }

    /**
     * 根据用户id获取用户
     */
    @Override
    public UmsAdmin getItem(Long id) {
        return umsAdminMapper.selectByPrimaryKey(id);
    }
}
