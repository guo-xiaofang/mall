package com.sanjutou.mall.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sanjutou.mall.dto.UmsMenuCreateParam;
import com.sanjutou.mall.entity.UmsMenu;
import com.sanjutou.mall.mapper.UmsMenuMapper;
import com.sanjutou.mall.service.UmsMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UmsMenuServiceImpl implements UmsMenuService {

    @Autowired
    private UmsMenuMapper umsMenuMapper;

    /**
     * 查询菜单树
     */
    @Override
    public List<UmsMenu> treeList() {
        List<UmsMenu> umsMenus = umsMenuMapper.selectList(null);
        List<UmsMenu> collect = umsMenus.stream()
                .filter(umsMenu -> umsMenu.getParentId().equals(0L))
                .map(umsMenuParent -> buildTree(umsMenuParent, umsMenus))
                .collect(Collectors.toList());
        return collect;
    }

    /**
     * 组织菜单树儿子节点
     */
    private UmsMenu buildTree(UmsMenu umsMenuParent, List<UmsMenu> umsMenus) {
        List<UmsMenu> children = umsMenus.stream()
                .filter(subMenu -> subMenu.getParentId().equals(umsMenuParent.getId()))
                .map(subMenu -> buildTree(subMenu, umsMenus)).collect(Collectors.toList());
        umsMenuParent.setChildren(children);
        return umsMenuParent;
    }

    /**
     * 分页查询后台菜单
     */
    @Override
    public IPage<UmsMenu> list(Long parentId, Integer pageSize, Integer pageNum) {
        LambdaQueryWrapper<UmsMenu> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(UmsMenu::getParentId, parentId);
        wrapper.orderByDesc(UmsMenu::getSort);
        IPage<UmsMenu> page = new Page<>(pageNum, pageSize);
        IPage<UmsMenu> umsMenuIPage = umsMenuMapper.selectPage(page, wrapper);
        return umsMenuIPage;
    }

    /**
     * 通过id查询菜单回显数据
     */
    @Override
    public UmsMenu oneById(Long id) {
        return umsMenuMapper.selectById(id);
    }

    /**
     * 新增菜单
     */
    @Override
    public int create(UmsMenuCreateParam umsMenuCreateParam) {
        UmsMenu umsMenu = new UmsMenu();
        BeanUtil.copyProperties(umsMenuCreateParam,umsMenu);
        if(umsMenuCreateParam.getParentId().equals(0L)){
            umsMenu.setLevel(0);
        }else{
            umsMenu.setLevel(1);
        }
        umsMenu.setCreateTime(new Date());
        return umsMenuMapper.insert(umsMenu);
    }
    /**
     * 修改菜单
     */
    @Override
    public int update(UmsMenuCreateParam umsMenuCreateParam) {
        UmsMenu umsMenu = new UmsMenu();
        BeanUtil.copyProperties(umsMenuCreateParam,umsMenu);
        if(umsMenuCreateParam.getParentId().equals(0L)){
            umsMenu.setLevel(0);
        }else{
            umsMenu.setLevel(1);
        }
        return umsMenuMapper.updateById(umsMenu);
    }

    /**
     * 删除指定菜单信息
     */
    @Override
    public int delete(Long id) {
        return umsMenuMapper.deleteById(id);
    }

    /**
     * 修改菜单前端是否隐藏
     */
    @Override
    public int updateHidden(Long id, Integer hidden) {
        return umsMenuMapper.updateHidden(id, hidden);
    }
}
