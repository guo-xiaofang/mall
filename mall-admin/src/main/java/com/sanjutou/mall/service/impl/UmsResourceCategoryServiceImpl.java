package com.sanjutou.mall.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sanjutou.mall.dto.UmsResourceCategoryCreateParam;
import com.sanjutou.mall.entity.UmsResource;
import com.sanjutou.mall.entity.UmsResourceCategory;
import com.sanjutou.mall.mapper.UmsResourceCategotyMapper;
import com.sanjutou.mall.service.UmsResourceCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UmsResourceCategoryServiceImpl implements UmsResourceCategoryService {

    @Autowired
    private UmsResourceCategotyMapper umsResourceCategoryMapper;

    /**
     * 获取所有资源分类
     */
    @Override
    public List<UmsResourceCategory> listAll() {
        LambdaQueryWrapper<UmsResourceCategory> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByDesc(UmsResourceCategory::getSort);
        return umsResourceCategoryMapper.selectList(wrapper);
    }

    /**
     * 修改资源分类
     */
    @Override
    public int update(UmsResourceCategoryCreateParam resourceCategoryCreateParam) {
        UmsResourceCategory umsResourceCategory = new UmsResourceCategory();
        BeanUtil.copyProperties(resourceCategoryCreateParam,umsResourceCategory);
        return umsResourceCategoryMapper.updateById(umsResourceCategory);
    }

    /**
     * 新增资源分类
     */
    @Override
    public int create(UmsResourceCategoryCreateParam resourceCategoryCreateParam) {
        UmsResourceCategory umsResourceCategory = new UmsResourceCategory();
        BeanUtil.copyProperties(resourceCategoryCreateParam,umsResourceCategory);
        umsResourceCategory.setCreateTime(new Date());
        return umsResourceCategoryMapper.insert(umsResourceCategory);
    }

    @Override
    public int delete(Long id) {
        return umsResourceCategoryMapper.deleteById(id);
    }
}
