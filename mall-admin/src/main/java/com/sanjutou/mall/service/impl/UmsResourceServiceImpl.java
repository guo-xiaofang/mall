package com.sanjutou.mall.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sanjutou.mall.dto.UmsResourceCreateParam;
import com.sanjutou.mall.entity.UmsResource;
import com.sanjutou.mall.mapper.UmsResourceMapper;
import com.sanjutou.mall.service.UmsResourceService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UmsResourceServiceImpl implements UmsResourceService {

    @Autowired
    private UmsResourceMapper umsResourceMapper;

    @Autowired
    private UmsAdminCacheServiceImpl umsAdminCacheService;
    /**
     * 查询全部资源
     */
    @Override
    public List<UmsResource> listAll() {
        return umsResourceMapper.selectList(null);
    }

    /**
     * 分页查询资源列表
     */
    @Override
    public IPage<UmsResource> listPage(String nameKeyword, String urlKeyword, Integer categoryId, Integer pageSize, Integer pageNum) {
        LambdaQueryWrapper<UmsResource> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StringUtils.isNotBlank(nameKeyword), UmsResource::getName, nameKeyword);
        wrapper.like(StringUtils.isNotBlank(urlKeyword), UmsResource::getUrl, urlKeyword);
        wrapper.eq(categoryId != null, UmsResource::getCategoryId, categoryId);
        IPage<UmsResource> page = new Page<>(pageNum, pageSize);
        IPage<UmsResource> umsResourceIPage = umsResourceMapper.selectPage(page, wrapper);
        return umsResourceIPage;
    }

    /**
     * 更新资源
     */
    @Override
    public int update(UmsResourceCreateParam umsResourceCre) {
        UmsResource umsResource = new UmsResource();
        BeanUtil.copyProperties(umsResourceCre,umsResource);
        umsAdminCacheService.delResourceListByResource(umsResourceCre.getId());
        return umsResourceMapper.updateById(umsResource);
    }

    /**
     * 新建资源
     */
    @Override
    public int create(UmsResourceCreateParam umsResourceCre) {
        UmsResource umsResource = new UmsResource();
        BeanUtil.copyProperties(umsResourceCre,umsResource);
        umsResource.setCreateTime(new Date());
        return umsResourceMapper.insert(umsResource);
    }

    /**
     * 删除指定资源信息
     */
    @Override
    public int delete(Long id) {
        umsAdminCacheService.delResourceListByResource(id);
        return umsResourceMapper.deleteById(id);
    }
}
