package com.sanjutou.mall.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sanjutou.mall.dto.UmsRoleCreateParam;
import com.sanjutou.mall.entity.*;
import com.sanjutou.mall.mapper.UmsRoleMapper;
import com.sanjutou.mall.mapper.UmsRoleMenuRelationMapper;
import com.sanjutou.mall.mapper.UmsRoleResourceRelationMapper;
import com.sanjutou.mall.service.UmsRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UmsRoleServiceImpl implements UmsRoleService {

    @Autowired
    private UmsRoleMapper umsRoleMapper;

    @Autowired
    private UmsRoleResourceRelationMapper umsRoleResourceRelationMapper;

    @Autowired
    private UmsRoleMenuRelationMapper umsRoleMenuRelationMapper;

    @Autowired
    private UmsAdminCacheServiceImpl umsAdminCacheService;


    @Override
    public List<UmsMenu> getMenuList(Long adminId) {
        return umsRoleMapper.getMenuList(adminId);
    }

    @Override
    public List<UmsRole> listAll() {
        return umsRoleMapper.selectList(null);
    }

    /**
     * 分页查询角色列表
     */
    @Override
    public IPage<UmsRole> listPage(String keyword, Integer pageSize, Integer pageNum) {
        QueryWrapper<UmsRole> wrapper = new QueryWrapper<>();
        wrapper.like(StrUtil.isNotBlank(keyword), "name", keyword);
        Page<UmsRole> page = new Page<>(pageNum, pageSize);
        return umsRoleMapper.selectPage(page, wrapper);
    }

    /**
     * 新增角色
     */
    @Override
    public int create(UmsRoleCreateParam umsRoleCre) {
        UmsRole umsRole = new UmsRole();
        BeanUtil.copyProperties(umsRoleCre, umsRole);
        umsRole.setCreateTime(new Date());
        umsRole.setAdminCount(0);
        umsRole.setAdminCount(0);
        return umsRoleMapper.insert(umsRole);
    }

    /**
     * 修改角色
     */
    @Override
    public int update(UmsRoleCreateParam umsRoleCre) {
        UmsRole umsRole = new UmsRole();
        BeanUtil.copyProperties(umsRoleCre, umsRole);
        return umsRoleMapper.updateById(umsRole);
    }

    /**
     * 删除角色
     */
    @Override
    public int delete(Long id) {
        umsAdminCacheService.delResourceListByRole(id);
        return umsRoleMapper.deleteById(id);
    }

    /**
     * 修改角色状态
     */
    @Override
    public int updateStatus(Long id, Integer status) {
        return umsRoleMapper.updateStatus(id, status);
    }

    /**
     * 查询该角色的菜单信息
     */
    @Override
    public List<UmsMenu> listMenuByRole(Long id) {
        return umsRoleMapper.listMenuByRole(id);
    }

    /**
     * 给角色分配菜单
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int allocMenu(Long roleId, List<Long> menuIds) {
        int count = menuIds == null ? 0 : menuIds.size();
        // 先删除原来的关系
        QueryWrapper<UmsRoleMenuRelation> wrapper = new QueryWrapper<>();
        wrapper.eq("role_id", roleId);
        umsRoleMenuRelationMapper.delete(wrapper);
        if (CollectionUtil.isNotEmpty(menuIds)) {
            List<UmsRoleMenuRelation> umsRoleMenuRelations = new ArrayList<>();
            for (Long menuId : menuIds) {
                UmsRoleMenuRelation umsRoleMenuRelation = new UmsRoleMenuRelation();
                umsRoleMenuRelation.setRoleId(roleId);
                umsRoleMenuRelation.setMenuId(menuId);
                umsRoleMenuRelations.add(umsRoleMenuRelation);
            }
            return umsRoleMenuRelationMapper.insertBatch(umsRoleMenuRelations);
        }
        return count;
    }

    /**
     * 给角色分配访问资源
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int allocResource(Long roleId, List<Long> resourceIds) {
        int count = resourceIds == null ? 0 : resourceIds.size();
        // 先删除原来的关系
        QueryWrapper<UmsRoleResourceRelation> wrapper = new QueryWrapper<>();
        wrapper.eq("role_id", roleId);
        umsRoleResourceRelationMapper.delete(wrapper);
        if (CollectionUtil.isNotEmpty(resourceIds)) {
            List<UmsRoleResourceRelation> umsRoleMenuRelations = new ArrayList<>();
            for (Long resourceId : resourceIds) {
                UmsRoleResourceRelation umsRoleResourceRelation = new UmsRoleResourceRelation();
                umsRoleResourceRelation.setRoleId(roleId);
                umsRoleResourceRelation.setResourceId(resourceId);
                umsRoleMenuRelations.add(umsRoleResourceRelation);
            }
            return umsRoleResourceRelationMapper.insertBatch(umsRoleMenuRelations);
        }
        umsAdminCacheService.delResourceListByRole(roleId);
        return count;
    }

    /**
     * 查询角色拥有的资源
     */
    @Override
    public List<UmsResource> listResourceByRole(Long id) {
        return umsRoleMapper.listResourceByRole(id);
    }
}
