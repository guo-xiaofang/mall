package com.sanjutou.mall;


import com.sanjutou.mall.mapper.UmsAdminRoleRelationMapper;
import com.sanjutou.mall.security.config.IgnoreUrlsConfig;
import com.sanjutou.mall.service.UmsAdminService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;


@SpringBootTest
public class MallAdminApplicationTest {

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    UmsAdminRoleRelationMapper umsAdminRoleRelationMapper;
    @Autowired
    IgnoreUrlsConfig ignoreUrlsConfig;
    @Autowired
    UmsAdminService umsAdminService;

    @Test
    void contextLoads() {
        System.out.println(passwordEncoder.encode("admin"));
        System.out.println(passwordEncoder.matches("admin", "$2a$10$bxlJMIzj9Z0fvkgBSJo6QebZngOw.xFv5Ipp28Ez2vMVqpyMtsCwW"));
    }

    @Test
    void getResourceListTest() {
        System.out.println(umsAdminRoleRelationMapper.getResourceList(new Long(2)));
    }

    @Test
    void getUrlsTest() {
        System.out.println(ignoreUrlsConfig.getUrls());

    }

    @Test
    void a() {

    }
}
