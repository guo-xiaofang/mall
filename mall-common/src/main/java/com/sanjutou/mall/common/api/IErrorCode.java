package com.sanjutou.mall.common.api;

/**
 * 常用API返回对象接口
 * Created by sunliang on 2022/6/11.
 */
public interface IErrorCode {
    /**
     * 返回码
     */
    long getCode();

    /**
     * 返回信息
     */
    String getMessage();
}
