package com.sanjutou.mall.common.exception;

import com.sanjutou.mall.common.api.CommonResult;
import com.sanjutou.mall.common.api.IErrorCode;
import com.sanjutou.mall.common.api.ResultCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.ServletOutputStream;

/**
 * 全局异常处理
 *
 * @author sunliang
 * @date 2022/6/16
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 捕获自定义业务异常
     * @param e
     * @return
     */
    @ExceptionHandler(value = ApiException.class)
    public CommonResult handle(ApiException e) {
        if (e.getErrorCode() != null) {
            return CommonResult.failed(e.getErrorCode());
        }
        log.error("发生业务异常！原因是：{}",e.getMessage());
        return CommonResult.failed(e.getMessage());
    }

    /**
     * 捕获方法参数异常
     * @param e
     * @return
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public CommonResult handleValidException(MethodArgumentNotValidException e) {
        BindingResult bindingResult = e.getBindingResult();
        String message = null;
        if (bindingResult.hasErrors()) {
            FieldError fieldError = bindingResult.getFieldError();
            if (fieldError != null) {
                message = fieldError.getField()+fieldError.getDefaultMessage();
            }
        }
        log.error("发生方法参数异常！原因是：{}",message);
        return CommonResult.validateFailed(message);
    }

    @ExceptionHandler(value = BindException.class)
    public CommonResult handleValidException(BindException e) {
        BindingResult bindingResult = e.getBindingResult();
        String message = null;
        if (bindingResult.hasErrors()) {
            FieldError fieldError = bindingResult.getFieldError();
            if (fieldError != null) {
                message = fieldError.getField()+fieldError.getDefaultMessage();
            }
        }
        log.error("发生参数绑定异常！原因是：{}",message);
        return CommonResult.validateFailed(message);
    }
    /**
     * 运行时异常
     * @param e
     * @return
     */
    @ResponseBody
    @ExceptionHandler(value = RuntimeException.class)
    public CommonResult handleArgsException(RuntimeException e){
        log.error("发生运行时异常！原因是：{}",e.getMessage());
        return CommonResult.failed(ResultCode.FAILED);
    }
}