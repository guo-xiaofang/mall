package com.sanjutou.mall.common.log;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.Duration;
import java.time.Instant;

@Slf4j
@Aspect
@Configuration
public class ServiceLogAspectConfig {

    @Pointcut("execution(public * com.sanjutou.mall..service.*.*(..))")
    public void serviceLog() {

    }

    @Around("serviceLog()")
    public Object doAroundService(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Instant instStart = Instant.now();
        Object object = proceedingJoinPoint.proceed();
        Instant instEnd = Instant.now();
        long timeDifference = Duration.between(instStart, instEnd).toMillis();
        BigDecimal divideUnsigned = BigDecimal.valueOf(timeDifference)
                .divide(BigDecimal.valueOf(1000L), new MathContext(2));

        log.info("-------------------start  {}-----------------", Thread.currentThread().getName());
        log.info("执行class_method={}, args={}",
                proceedingJoinPoint.getSignature().getDeclaringTypeName() + "." + proceedingJoinPoint
                        .getSignature().getName(), proceedingJoinPoint.getArgs());
        log.info("{}请求耗时={}秒", Thread.currentThread().getName(), divideUnsigned);
        log.info("返回值{}", object);
        log.info("-------------------{}  end-----------------", Thread.currentThread().getName());
        return object;
    }

}

